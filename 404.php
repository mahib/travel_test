<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */
get_header(); ?>

<!-- post-top start-->
	<section id="blog-top">
		<div class="travel-blog-top-content" style="background: url(<?php 
		if(cs_get_option('blog_top_img')) {
			echo esc_url(cs_get_option('blog_top_img'));
		}
		?>)">
			
			<div class="container">
			   <h1><?php echo esc_html(cs_get_option('top_title'));?></h1>      
			   <ol class="travel-breadcrumb breadcrumb">
				  <?php if (function_exists('travel_custom_breadcrumbs')) travel_custom_breadcrumbs(); ?>
			   </ol>
			</div>      
		 <div class="overlay"></div>
		</div>
   </section>
   <!-- post-top end-->

	<!-- Start blog -->		
<div id="post-section" >
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="travel-blog-posts ">
					<section class="error-404 not-found">
						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'travel' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'travel' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .page-content -->
					</section><!-- .error-404 -->
				</div>
			</div>
		</div><!-- /.container -->
	</div><!-- /.container -->
</div><!-- /.blogg -->
<?php get_footer(); ?>