<!-- Start Footer -->

	<section id="travel-footer">
		<div class="travel-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-8 text-left">
						<p>
						<?php 
							if(empty((cs_get_option('author_title')) && (cs_get_option('reserved_title')))){
								
								echo esc_html(' @Helloxpart ').esc_html(' All Right Reserve') ;
								
							}else{?><a href="<?php echo esc_url(cs_get_option('author_link'));?>"><?php echo esc_html(cs_get_option('author_title'));?></a>
							<?php echo esc_html(cs_get_option('reserved_title'));}?>
						</p>
					</div>
					
					<div class="col-md-4 col-sm-4 text-right">
						<div class="social-icons">
							<?php
								$foo_icon = cs_get_option('icon_group');
								if(is_array($foo_icon)){
									foreach($foo_icon as $key => $value){?>
									<a href="<?php echo esc_url($value['foo_icon_link']);?>"><i class="<?php echo esc_attr($value['footer_icon']);?>"></i></a>	
									<?php }
								}
							?>	
						</div><!-- /.social-icons -->
					</div>
					
				</div>
			</div>
			<a class="ScrollUp" id="travelScrollUp" href="#">
				<i class="fa fa-arrow-circle-up"></i>
			</a>
		</div>
	</section>
	<!-- End Footer -->
		<?php wp_footer();?>
</body>
</html>