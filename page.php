<?php get_header(); ?>	
<div id="post-section" >
	<div class="container">
		<div class="col-md-8 col-sm-8">
			<div class="row">
				<div class="travel-blog-posts ">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						// Include the page content template.
									the_content();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
						// End of the loop.
					endwhile;
					?>
				</div>
			</div><!-- row -->
		</div><!-- col -->
	</div><!-- containenr -->
</div><!-- Blog_single_p_page_blog -->
<?php get_footer(); ?>