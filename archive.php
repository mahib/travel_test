<?php get_header();?>
<!-- post-top start-->
	<section id="blog-top">
		<div class="travel-blog-top-content" style="background: url(<?php 
		if(cs_get_option('blog_top_img')) {
			echo esc_url(cs_get_option('blog_top_img'));
		}
		?>)">
			<div class="container">
				<header class="page-header">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header><!-- .page-header -->
				<ol class="travel-breadcrumb breadcrumb">
				  <?php if (function_exists('travel_custom_breadcrumbs')) travel_custom_breadcrumbs(); ?>
				</ol>
			</div>      
			<div class="overlay"></div>
		</div>
   </section>
   <!-- post-top end-->	
	<!-- post-section start-->
   <div id="post-section" >
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="travel-blog-posts singlepage">				
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_format() );
							// End the loop.
							endwhile;
							// Previous/next page navigation.
							?>
							<nav aria-label="..." class="text-center">
							  <ul class="travel-pagination pagination">									
								<?php
									// Previous/next page navigation.
									echo paginate_links( array(
										'prev_text'          => __( '<span class="fa fa-angle-left" aria-hidden="true"></span>', 'travel' ),
										'next_text'          => __( '<span class="fa fa-angle-right" aria-hidden="true"></span>', 'travel' ),									
									) );
								?>
							  </ul><!-- /.travel-pagination pagination -->
							</nav><?php
						// If no content, include the "No posts found" template.
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif;
						?>
					</div><!-- /.blog-posts -->
				</div><!-- /.col-md-8 -->
				<?php get_sidebar(); ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div>
	<!-- post-section end -->
<?php get_footer();?>