jQuery(document).ready(function(jQuery) {
	
	//metabox display depenedency on post format
	function resetmetaBox() {
		jQuery(".postbox div.cs-metabox-framework").each(function () {

			//post format
			var dependency_post_format = jQuery(this).find('.cs-fieldset input').attr('data-format'); 
			// console.log(dependency_post_format);
			var selected_post_type = jQuery("#post-formats-select input:checked").attr("id"); 
			if (selected_post_type) {
				if (selected_post_type == "post-format-0") selected_post_type = "post-format-standard";
				if (dependency_post_format.indexOf(selected_post_type.replace("post-format-", "")) == -1) {
					jQuery(this).parent().parent().hide();
				} else {
					jQuery(this).parent().parent().show();
				}
			}
		});
	}
	jQuery("#post-formats-select input").on("change",resetmetaBox);
    resetmetaBox();
});