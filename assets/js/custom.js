jQuery(document).ready(function () {
	"use strict";
	/* ==================================
	 Table of contents:-
		1. Preloader
		2. Side Menu  portfolio
		3. progress-bar and tooltip
		4. one page scrolling  
		5. Scroll To Top 
		6. wow.js active 
		7. counter up active 
		8. magnificPopup active
		9. rev-slider -
			9-1. Agency
			9-2. Architecture 
			9-3. Photography 
			9-4. Default 
			9-5. Medical 
			9-6. Demo page
			9-7. Portfolio
			9-8. default 
			9-9. medical 
			9-10. yoga
			9-11. spa
			9-12. restaurant
			9-13. fashion
			9-14. travel
		10. Isotope and Masonry -
			10-1. agency  
			10-2. portfolio  
			10-3. photography-portfolio 
			10-4. photography  
			10-5. default 
			10-6. medical department 
			10-7. medical portfolio
			10-8. yoga
			10-9. spa
			10-10. restaurant
			10-11. fashion
		11. OwlCarousel -
			11-1. agency 
			11-2. portfolio 
			11-3. architect 
			11-4. photography  
			11-5. default 
			11-6. default-client-carousel 
			11-7. medical doctors 
			11-8. medical testimonial
			11-9. spa testimonial
			11-10. travel testimonial
			11-11. restaurant testimonial
			11-12. fashion service
			11-13. fashion blog
		12. Gmap -
			12-1. Medical
			12-2. Agency
			12-3. Portfolio
			12-4. Restaurant
		13. bxslider
		14. datetimepicker				
	================================== */
	/*----------------------------
		1. Preloader
	-------------------------------*/
	jQuery(window).on('load', function () {
		jQuery("#spinningSquaresG1").delay(1000).fadeOut(500);
		jQuery(".inTurnBlurringTextG").on('click', function () {
			jQuery("#spinningSquaresG1").fadeOut(500);
		});
	});
	/*----------------------------
		2. Side Menu  portfolio 
	-------------------------------*/
	jQuery(".main-menu.portfolio .menu-bar .menu-icon").on("click", function () {
		jQuery(".menu-box").toggleClass("show");
	});
	var icon1 = 'assets/images/pfolio/menu-icon.png';
	var icon2 = 'assets/images/pfolio/menu-icon-close.png';
	jQuery('.main-menu.portfolio .menu-icon').on("click", function () {
		if (jQuery('.icon-toggle').attr('src') === icon1) {
			jQuery('.icon-toggle').attr('src', icon2);
		} else {
			jQuery('.icon-toggle').attr('src', icon1)
		}
	});
	/*-------------------------------
		3. progress-bar and tooltip 
	--------------------------------- */
	var dataToggleTooTip = jQuery('[data-toggle="tooltip"]');
	var progressBar = jQuery(".progress-bar");
	if (progressBar.length) {
		progressBar.appear(function () {
			dataToggleTooTip.tooltip({
				trigger: 'manual'
			}).tooltip('show');
			progressBar.each(function () {
				var each_bar_width = jQuery(this).attr('aria-valuenow');
				jQuery(this).width(each_bar_width + '%');
			});
		});
	}
	/*---------------------
		4. one page scrolling 
	-------------------------*/
	jQuery('a.page-scroll').on('click', function (event) {
		var jQueryanchor = jQuery(this);
		jQuery('html, body').stop().animate({
			scrollTop: jQuery(jQueryanchor.attr('href')).offset().top
		}, 1000, 'easeInSine');
		event.preventDefault();
	});
	/*----------------------
		5. Scroll To Top    
	-----------------------*/
	jQuery(window).on('scroll', function () {
		if (jQuery(this).scrollTop() > 600) {
			jQuery('.ScrollUp').fadeIn();
		} else {
			jQuery('.ScrollUp').fadeOut();
		}
	});
	jQuery('.ScrollUp').on('click', function () {
		jQuery('html, body').animate({
			scrollTop: 0
		}, 1500);
		return false;
	});
	/*---------------------
		6. wow js active  
	----------------------- */
	new WOW().init();
	/* --------------------
		7. counter up active  
	---------------------- */
	jQuery('.counter').counterUp({
		delay: 10,
		time: 5000
	});
	/*---------------------
	 8. magnificPopup active 
	--------------------- */
	// jQuery("a[rel^='prettyPhoto']").prettyPhoto();
	jQuery('.portfolio-item').magnificPopup({
		delegate: 'a', // child items selector, by clicking on it popup will open
		type: 'image',
		gallery: {
			enabled: true
		}
	});
	// jQuery("a[rel^='prettyPhoto']").prettyPhoto();
	jQuery('.image').magnificPopup({
		delegate: 'a', // child items selector, by clicking on it popup will open
		type: 'image',
		gallery: {
			enabled: true
		}
	});
	/*-------------------------------
	 yoga-video magnificPopup  
	-------------------------------*/
	jQuery('.yoga-video-single .video a').magnificPopup({
		disableOn: 0,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: true,
		fixedContentPos: false
	});
	/* ----------------------
		9-1. rev-slider Agency 
	------------------------- */
	var monty = jQuery;
	var revapi1014;
	if (monty("#agency_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#agency_rev_slider");
	} else {
		revapi1014 = monty("#agency_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "uranus",
					enable: false,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [900, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "off",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* -------------------------- 
		9-2. rev-slider Architecture 
	--------------------------------- */
	var monty = jQuery;
	var revapi1016;
	if (monty("#architect_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#architect_rev_slider");
	} else {
		revapi1016 = monty("#architect_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "persephone",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "right",
						v_align: "bottom",
						h_offset: 40,
						v_offset: 90
					},
					right: {
						h_align: "right",
						v_align: "bottom",
						h_offset: 40,
						v_offset: 40
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [900, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "on",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ---------------------------
		9-3. rev-slider Photography  
	--------------------------- */
	var monty = jQuery;
	var revapi1017;
	if (monty("#photography_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#photography_rev_slider");
	} else {
		revapi1017 = monty("#photography_rev_slider").show().revolution({
			liderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "persephone",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 280,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1240, 1024, 778, 480],
			gridheight: [800, 768, 960, 720],
			lazyType: "none",
			shadow: 0,
			spinner: "spinner4",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* -----------------------------
		9-4. rev-slider Default   
	----------------------------- */
	var monty = jQuery;
	var revapi1018;
	if (monty("#default_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#default_rev_slider");
	} else {
		revapi1018 = monty("#default_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "persephone",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [900, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "spinner3",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ------------------------------------
		9-5. rev-slider Medical 
	--------------------------------------- */
	var monty = jQuery;
	var revapi1015;
	if (monty("#medical_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#medical_rev_slider");
	} else {
		revapi1015 = monty("#medical_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 6000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "persephone",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [900, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "spinner2",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ------------------------------------
		9-6. rev-slider Demo page 
	--------------------------------------- */
	var monty = jQuery;
	var revapi484;
	if (monty("#rev_slider_1").revolution == undefined) {
		revslider_showDoubleJqueryError("#rev_slider_1");
	} else {
		revapi484 = monty("#rev_slider_1").show().revolution({
			sliderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 4000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1240, 1024, 778, 480],
			gridheight: [800, 768, 960, 720],
			lazyType: "single",
			parallax: {
				origo: "slidercenter",
				speed: 300,
				levels: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 49, 50, 51, 55],
				type: "mouse",
			},
			shadow: 0,
			spinner: "spinner2",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/*----------------------------------------
		9-7. revolution slider portfolio 
	-----------------------------------------*/
	var monty = jQuery;
	var revapi484;
	if (monty("#portfolio_rev_slider").revolution == undefined) {
		revslider_showDoubleJqueryError("#portfolio_rev_slider");
	} else {
		revapi484 = monty("#portfolio_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 4000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1240, 1024, 778, 480],
			gridheight: [600, 768, 960, 720],
			lazyType: "single",
			parallax: {
				origo: "slidercenter",
				speed: 300,
				levels: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 49, 50, 51, 55],
				type: "mouse",
			},
			shadow: 0,
			spinner: "off",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ----------------------------
	 rev-slider yoga
	------------------------------- */
	var tpj = jQuery;
	var revapi1014;
	if (tpj("#yoga_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#yoga_rev_slider");
	} else {
		revapi1014 = tpj("#yoga_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "uranus",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [900, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "off",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ----------------------------
	 rev-slider spa
	------------------------------- */
	var monty = jQuery;
	var revapi1014;
	if (monty("#spa_rev_slider").revolution == undefined) {
		// revslider_showDoubleJqueryError("#spa_rev_slider");
	} else {
		revapi1014 = monty("#spa_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				bullets: {
					enable: true,
					style: 'hesperiden',
					tmp: '',
					direction: 'horizontal',
					rtl: false,
					container: 'slider',
					h_align: 'center',
					v_align: 'bottom',
					h_offset: 0,
					v_offset: 20,
					space: 10,
					hide_onleave: false,
					hide_onmobile: false,
					hide_under: 0,
					hide_over: 9999,
					hide_delay: 200,
					hide_delay_mobile: 1200
				},
				arrows: {
					style: "uranus",
					enable: false,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1600, 1024, 778, 480],
			gridheight: [1000, 700, 500, 400],
			lazyType: "none",
			shadow: 0,
			spinner: "off",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "60px",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ------------------------------------
	 rev-slider Travel 
	--------------------------------------- */
	var monty = jQuery;
	var revapi484;
	if (monty("#travel_rev_slider").revolution == undefined) {
		revslider_showDoubleJqueryError("#travel_rev_slider");
	} else {
		revapi484 = monty("#travel_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 4000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "uranus",
					enable: true,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1240, 1024, 778, 480],
			gridheight: [800, 768, 960, 720],
			lazyType: "single",
			parallax: {
				origo: "slidercenter",
				speed: 300,
				levels: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 49, 50, 51, 55],
				type: "mouse",
			},
			shadow: 0,
			spinner: "spinner2",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ------------------------------------
	 rev-slider Restaurant 
	--------------------------------------- */
	var monty = jQuery;
	var revapi484;
	if (monty("#restaurant_rev_slider").revolution == undefined) {
		revslider_showDoubleJqueryError("#restaurant_rev_slider");
	} else {
		revapi484 = monty("#restaurant_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 7000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				bullets: {
					enable: true,
					style: 'hesperiden',
					tmp: '',
					direction: 'horizontal',
					rtl: false,
					container: 'slider',
					h_align: 'center',
					v_align: 'bottom',
					h_offset: 0,
					v_offset: 50,
					space: 10,
					hide_onleave: false,
					hide_onmobile: false,
					hide_under: 0,
					hide_over: 9999,
					hide_delay: 200,
					hide_delay_mobile: 1200
				},
				arrows: {
					style: "uranus",
					enable: false,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1240, 1024, 778, 480],
			gridheight: [800, 768, 960, 720],
			lazyType: "single",
			parallax: {
				origo: "slidercenter",
				speed: 300,
				levels: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 49, 50, 51, 55],
				type: "mouse",
			},
			shadow: 0,
			spinner: "spinner2",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* ------------------------------------
	 rev-slider Fashion 
	--------------------------------------- */
	var monty = jQuery;
	var revapi484;
	if (monty("#fashion_rev_slider").revolution == undefined) {
		revslider_showDoubleJqueryError("#fashion_rev_slider");
	} else {
		revapi484 = monty("#fashion_rev_slider").show().revolution({
			sliderType: "standard",
			jsFileLocation: "js",
			sliderLayout: "fullscreen",
			dottedOverlay: "none",
			delay: 7000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				bullets: {
					enable: true,
					style: 'hesperiden',
					tmp: '',
					direction: 'horizontal',
					rtl: false,
					container: 'slider',
					h_align: 'center',
					v_align: 'bottom',
					h_offset: 0,
					v_offset: 50,
					space: 10,
					hide_onleave: false,
					hide_onmobile: false,
					hide_under: 0,
					hide_over: 9999,
					hide_delay: 200,
					hide_delay_mobile: 1200
				},
				arrows: {
					style: "uranus",
					enable: false,
					hide_onmobile: true,
					hide_under: 768,
					hide_onleave: false,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			responsiveLevels: [1240, 1024, 778, 480],
			visibilityLevels: [1240, 1024, 778, 480],
			gridwidth: [1366, 1024, 778, 480],
			gridheight: [800, 768, 960, 720],
			lazyType: "single",
			parallax: {
				origo: "slidercenter",
				speed: 300,
				levels: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 49, 50, 51, 55],
				type: "mouse",
			},
			shadow: 0,
			spinner: "spinner2",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: 0,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
		});
	}
	/* -------------------------------
	 10-1. masonry agency 
	-------------------------------- */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".gallery_items").isotope({
			itemSelector: '.single_item',
			masonry: {
				columnWidth: '.single_item_sizer25'
			}
		});
		// Add isotope click function
		jQuery('.gallery_filter li').on('click', function () {
			jQuery(".gallery_filter li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".gallery_items").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* --------------------
	 10-2. masonry portfolio   
	------------------------ */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".gallery_items.portfolio").isotope({
			itemSelector: '.single_item',
			masonry: {
				columnWidth: '.single_item_sizer_portfolio'
			}
		});
		// Add isotope click function
		jQuery('.gallery_filter.portfolio li').on('click', function () {
			jQuery(".gallery_filter.portfolio li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".gallery_items.portfolio").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* ------------------------------
	 10-3. masonry photography-portfolio 
	--------------------------------- */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		var jQuerycontainer = jQuery('.photography_portfolioContainer');
		jQuerycontainer.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		// Add isotope click function
		jQuery('.photography_portfolioFilter a').on('click', function () {
			jQuery(".photography_portfolioFilter a").removeClass("current");
			jQuery(this).addClass("current");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".photography_portfolioContainer ").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* -----------------------------------
	 10-5. masonry default  
	---------------------------------------- */
	jQuery(window).on('load', function () {
		var jQuerycontainer = jQuery('.default_portfolioContainer');
		jQuerycontainer.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		// Add isotope click function
		jQuery('.default_portfolioFilter a').on('click', function () {
			jQuery(".default_portfolioFilter a").removeClass("current");
			jQuery(this).addClass("current");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".default_portfolioContainer").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".medical_tab_items").isotope({
			itemSelector: '.single_item',
			masonry: {
				columnWidth: '.single_item_sizer33'
			}
		});
	});
	/* ---------------------------------------
	 10-7. masonry medical portfolio 
	------------------------------------------ */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".medical_portfolio_items").isotope({
			itemSelector: '.medi_port_single_item',
			masonry: {
				columnWidth: '.single_item_sizer33'
			}
		});
		// Add isotope click function
		jQuery('.medical_portfolio_filter li').on('click', function () {
			jQuery(".medical_portfolio_filter li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".medical_portfolio_items").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* ----------------------
	 yoga-masonry 
	----------------------- */
	jQuery(window).on('load', function () {
		if (jQuery('.yoga-container').length) {
			var jQuerycontainer = jQuery('.yoga-container').isotope({
				itemSelector: '.yoga-item',
				masonry: {
					columnWidth: '.yoga-sizer'
				}
			});
		}
	});
	/* --------------------
	   Spa gallery Masonry 
	  ------------------------ */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".spa_gallery_items").isotope({
			itemSelector: '.single_item',
			masonry: {
				columnWidth: '.spa_single_item_sizer'
			}
		});
		// Add isotope click function
		jQuery('.spa_gallery_filter li').on('click', function () {
			jQuery(".spa_gallery_filter li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".spa_gallery_items").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* ------------------------------
	 masonry Restaurant
	--------------------------------- */
	jQuery(window).on('load', function () {
		// Add isotope click function
		jQuery('.restaurant_gallery_filter li').on('click', function () {
			jQuery(".restaurant_gallery_filter li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".restaurant_gallery_items").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* -------------------------------
	 10-1. masonry Fashion 
	-------------------------------- */
	jQuery(window).on('load', function () {
		// Activate isotope in container         
		jQuery(".fashion_gallery_items").isotope({
			itemSelector: '.single_item',
			masonry: {
				columnWidth: '.item_sizer_33'
			}
		});
		// Add isotope click function
		jQuery('.fashion_gallery_filter li').on('click', function () {
			jQuery(".fashion_gallery_filter li").removeClass("active");
			jQuery(this).addClass("active");
			var selector = jQuery(this).attr('data-filter');
			jQuery(".fashion_gallery_items").isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'easing',
					queue: false,
				}
			});
			return false;
		});
	});
	/* --------------------------------
		11-1. owlCarousel agency 
	 ---------------------------------- */
	jQuery("#agency-owl-carousel").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		dots: false,
		dotsEach: false,
		nav: false,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		animateIn: 'fadeInOut',
		animateOut: 'fadeOut',
		smartSpeed: 3000
	});
	/* ---------------------
		11-2. owlCarousel portfolio 
		------------------------ */
	jQuery("#portfolio-owl").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: false,
		nav: false,
		navText: ["<i class='fa fa-angle-up'></i>", "<i class='fa fa-angle-down'></i>"],
		dots: true,
		dotsEach: true,
		animateOut: 'fadeInDown',
		smartSpeed: 1500,
		autoplayHoverPause: true,
	});
	/* --------------------
		11-3. owlCarousel architect 
		------------------------ */
	jQuery("#architect-owl-carousel").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: false,
		dots: true,
		dotsEach: true,
		smartSpeed: 1000,
		autoplayHoverPause: true,
	});
	/* --------------------
		 11-4. owlCarousel photography 
		------------------------ */
	jQuery("#photography-owl-carousel").owlCarousel({
		items: 1,
		autoplay: false,
		loop: true,
		mouseDrag: true,
		dots: false,
		dotsEach: false,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		animateOut: 'fadeOut',
		smartSpeed: 4000
	});
	/* -------------------------
		 11-5. owlCarousel default 
		----------------------------- */
	jQuery("#default-owl-carousel").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: true,
		navText: ["<i class='fa fa-angle-up'></i>", "<i class='fa fa-angle-down'></i>"],
		dots: false,
		dotsEach: false,
		animateOut: 'fadeOutUp',
		smartSpeed: 1500,
	});
	/* ------------------------------------
		 11-6. owlCarousel default-client-carousel 
		-------------------------------------- */
	jQuery("#default-client-carousel").owlCarousel({
		items: 4,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: false,
		navText: false,
		dots: false,
		dotsEach: false,
		smartSpeed: 800,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 4
			}
		}
	});
	/* -------------------------------------------
		11-7. owlCarousel medical doctors 
	 -------------------------------------------- */
	jQuery("#medical-doctors-owl").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: true,
		navText: ["<i class='fa fa-angle-up'></i>", "<i class='fa fa-angle-down'></i>"],
		dots: false,
		dotsEach: false,
		animateOut: 'fadeOut',
		smartSpeed: 1500,
	});
	/* ------------------------------------------
		11-8. owlCarousel medical testimonial 
		 --------------------------------------------- */
	jQuery("#medical-testimonial").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		dots: true,
		dotsEach: true,
		transitionStyle: "fade",
		smartSpeed: 1500,
	});
	/* --------------------
	    spa-testimonial owl-carousel 
	   ------------------------ */
	jQuery("#spa-testimonial").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		dots: false,
		mouseDrag: true,
		nav: false,
		transitionStyle: "fade",
		animateOut: 'fadeOut',
		smartSpeed: 1500,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	});
	/* ------------------------------------------
		 owlCarousel travel testimonial 
		--------------------------------------------- */
	jQuery("#travel-owl-carousel").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		dots: true,
		transitionStyle: "fade",
		animateOut: 'fadeOutLeft',
		smartSpeed: 1000,
	});
	/* ------------------------------------------
	 owlCarousel restaurant-owl-carousel
	--------------------------------------------- */
	jQuery("#restaurant-owl-carousel").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: false,
		dots: true,
		animateOut: 'fadeOutUp',
		smartSpeed: 3000,
	});
	/* ------------------------------------------
	 owlCarousel fashion-service-owl
	--------------------------------------------- */
	jQuery("#fashion-service-owl").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		dots: false,
		dotsEach: false,
		animateOut: 'fadeOut',
		smartSpeed: 1500,
	});
	/* ------------------------------------------
	 owlCarousel fashion-blog-owl
	--------------------------------------------- */
	jQuery("#fashion-blog-owl").owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		mouseDrag: true,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		dots: false,
		dotsEach: false,
		animateOut: 'fadeOutRight',
		smartSpeed: 1500,
	});
	/*----------------------------
		Gmaps Active Agency
	------------------------------ */
	var map;
	jQuery('.map_agency').each(function () {
		var element = jQuery(this).attr('id');
		map = new GMaps({
			el: '#' + element,
			center: new google.maps.LatLng(58.4847774, 43.4049086),
			zoom: 3,
			scrollwheel: false,
			styles: [{
				"featureType": "administrative",
				"elementType": "all",
				"stylers": [{
					"saturation": "-100"
				}]
			}, {
				"featureType": "administrative.province",
				"elementType": "all",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": 65
				}, {
					"visibility": "on"
				}]
			}, {
				"featureType": "poi",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": "50"
				}, {
					"visibility": "simplified"
				}]
			}, {
				"featureType": "road",
				"elementType": "all",
				"stylers": [{
					"saturation": "-100"
				}]
			}, {
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [{
					"visibility": "simplified"
				}]
			}, {
				"featureType": "road.arterial",
				"elementType": "all",
				"stylers": [{
					"lightness": "30"
				}]
			}, {
				"featureType": "road.local",
				"elementType": "all",
				"stylers": [{
					"lightness": "40"
				}]
			}, {
				"featureType": "transit",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"visibility": "simplified"
				}]
			}, {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [{
					"hue": "#ffff00"
				}, {
					"lightness": -25
				}, {
					"saturation": -97
				}]
			}, {
				"featureType": "water",
				"elementType": "labels",
				"stylers": [{
					"lightness": -25
				}, {
					"saturation": -100
				}]
			}]
		});
	});
	/*----------------------------
		Gmaps Active Medical 
	------------------------------ */
	var map;
	jQuery('.map_medial').each(function () {
		var element = jQuery(this).attr('id');
		map = new GMaps({
			el: '#' + element,
			center: new google.maps.LatLng(38.2260748, -97.0709008),
			zoom: 5,
			scrollwheel: false,
		});
		map.addMarker({
			lat: 37.0736973,
			lng: -96.0728027,
			title: 'United States',
			icon: 'assets/images/medical/map-marker.png',
		});
	});
	/*----------------------------
		Gmaps Active Portfolio 
	------------------------------ */
	var map;
	jQuery('.map_portfolio').each(function () {
		var element = jQuery(this).attr('id');
		map = new GMaps({
			el: '#' + element,
			center: new google.maps.LatLng(59.3381325, 13.3886198),
			zoom: 10,
			scrollwheel: false,
			styles: [{
				"featureType": "administrative",
				"elementType": "all",
				"stylers": [{
					"saturation": "-100"
				}]
			}, {
				"featureType": "administrative.province",
				"elementType": "all",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": 65
				}, {
					"visibility": "on"
				}]
			}, {
				"featureType": "poi",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": "50"
				}, {
					"visibility": "simplified"
				}]
			}, {
				"featureType": "road",
				"elementType": "all",
				"stylers": [{
					"saturation": "-100"
				}]
			}, {
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [{
					"visibility": "simplified"
				}]
			}, {
				"featureType": "road.arterial",
				"elementType": "all",
				"stylers": [{
					"lightness": "30"
				}]
			}, {
				"featureType": "road.local",
				"elementType": "all",
				"stylers": [{
					"lightness": "40"
				}]
			}, {
				"featureType": "transit",
				"elementType": "all",
				"stylers": [{
					"saturation": -100
				}, {
					"visibility": "simplified"
				}]
			}, {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [{
					"hue": "#ffff00"
				}, {
					"lightness": -25
				}, {
					"saturation": -97
				}]
			}, {
				"featureType": "water",
				"elementType": "labels",
				"stylers": [{
					"lightness": -25
				}, {
					"saturation": -100
				}]
			}]
		});
	});
	/*----------------------------
	 Gmaps Active Restaurant 
	------------------------------ */
	var map;
	jQuery('.map_restaurant').each(function () {
		var element = jQuery(this).attr('id');
		map = new GMaps({
			el: '#' + element,
			center: new google.maps.LatLng(40.7504864, -74.0014333),
			zoom: 5,
			scrollwheel: false,
		});
		map.addMarker({
			lat: 40.7587442,
			lng: -73.9808623,
			title: 'Dhaka',
			icon: 'assets/images/restaurant/map-marker.png',
		});
	});
	/*--------------------------------
 		fashion model bxslider
	---------------------------------*/
	if (jQuery('.product-gallery .bxslider').length > 0) {
		jQuery('.product-gallery .bxslider').bxSlider({
			pagerCustom: '.product-gallery #bx-pager',
			nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
			prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
			pager: true,
			speed: 1000,
			mode: 'fade'
		});
	}
	/*----------------------------
	 Datetimepicker 
	---------------------------*/
	jQuery('#datetimepicker1').datetimepicker();
	jQuery('.datepicker').datepicker({
		startDate: '-3d'
	});
	/* ------------------------------------------  the end ---------------------------------------------*/
});