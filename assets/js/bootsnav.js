// ------------------------------------------------------------------------------ //
//
// Template name : Bootsnav - Multi Purpose Header
// Categorie : Bootstrap Menu in CSS
// Author : adamnurdin01
// Version : v.1.2
// Created : 2016-06-02
// Last update : 2016-10-19
//
// ------------------------------------------------------------------------------ //

(function (jQuery) {
	"use strict";
    
    var bootsnav = {
        initialize: function() {
            this.event();
            this.hoverDropdown();
            this.navbarSticky();
            this.navbarScrollspy();
        },
        event : function(){
            
            // ------------------------------------------------------------------------------ //
            // Variable
            // ------------------------------------------------------------------------------ //
            var getNav = jQuery("nav.navbar.bootsnav");
            
            // ------------------------------------------------------------------------------ //
            // Navbar Sticky 
            // ------------------------------------------------------------------------------ //
            var navSticky = getNav.hasClass("navbar-sticky");
            if( navSticky ){
                // Wraped navigation
                getNav.wrap("<div class='wrap-sticky'></div>");
            }   
            
            // ------------------------------------------------------------------------------ //
            // Navbar Center 
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("brand-center")){                
                var postsArr = new Array(),
                    index = jQuery("nav.brand-center"),
                    jQuerypostsList = index.find('ul.navbar-nav');
				
				index.prepend("<span class='storage-name' style='display:none;'></span>");
                
                //Create array of all posts in lists
                index.find('ul.navbar-nav > li').each(function(){
					if( jQuery(this).hasClass("active") ){
						var getElement = jQuery("a", this).eq(0).text();
						jQuery(".storage-name").html(getElement);
					}
                    postsArr.push(jQuery(this).html());
                });
                
                //Split the array at this point. The original array is altered.
                var firstList = postsArr.splice(0, Math.round(postsArr.length / 2)),
                    secondList = postsArr,
                    ListHTML = '';
                
                var createHTML = function(list){
                    ListHTML = '';
                    for (var i = 0; i < list.length; i++) {
                        ListHTML += '<li>' + list[i] + '</li>'
                    }
                }
                
                //Generate HTML for first list
                createHTML(firstList);
                jQuerypostsList.html(ListHTML);
                index.find("ul.nav").first().addClass("navbar-left");
                
                //Generate HTML for second list
                createHTML(secondList);
                //Create new list after original one
                jQuerypostsList.after('<ul class="nav navbar-nav"></ul>').next().html(ListHTML);
                index.find("ul.nav").last().addClass("navbar-right");
                
                //Wrap navigation menu
                index.find("ul.nav.navbar-left").wrap("<div class='col-half left'></div>");
                index.find("ul.nav.navbar-right").wrap("<div class='col-half right'></div>");
                
                //Selection Class
                index.find('ul.navbar-nav > li').each(function(){ 
                    var dropDown = jQuery("ul.dropdown-menu", this),
                        megaMenu = jQuery("ul.megamenu-content", this);
                    dropDown.closest("li").addClass("dropdown");
                    megaMenu.closest("li").addClass("megamenu-fw");
                });
				
				var getName = jQuery(".storage-name").html();
				if( !getName == ""  ){
					jQuery( "ul.navbar-nav > li:contains('" + getName + "')" ).addClass("active");
				}		
            } 
            
            
            // ------------------------------------------------------------------------------ //
            // Navbar Sidebar 
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("navbar-sidebar")){
                // Add Class to body
                jQuery("body").addClass("wrap-nav-sidebar");
                getNav.wrapInner("<div class='scroller'></div>");
            }else{
                jQuery(".bootsnav").addClass("on");
            }
            
            // ------------------------------------------------------------------------------ //
            // Menu Center 
            // ------------------------------------------------------------------------------ //
            if( getNav.find("ul.nav").hasClass("navbar-center")){
                getNav.addClass("menu-center");
            }
            
            // ------------------------------------------------------------------------------ //
            // Navbar Full
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("navbar-full")){
                // Add Class to body
                jQuery("nav.navbar.bootsnav").find("ul.nav").wrap("<div class='wrap-full-menu'></div>");
                jQuery(".wrap-full-menu").wrap("<div class='nav-full'></div>");
                jQuery("ul.nav.navbar-nav").prepend("<li class='close-full-menu'><a href='#'><i class='fa fa-times'></i></a></li>");
            }else if( getNav.hasClass("navbar-mobile")){
                getNav.removeClass("no-full");
            }else{
                getNav.addClass("no-full");
            }
                
            // ------------------------------------------------------------------------------ //
            // Navbar Mobile
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("navbar-mobile")){
                // Add Class to body
                jQuery('.navbar-collapse').on('shown.bs.collapse', function() {
                    jQuery("body").addClass("side-right");
                });
                jQuery('.navbar-collapse').on('hide.bs.collapse', function() {
                    jQuery("body").removeClass("side-right");
                });
                
                jQuery(window).on("resize", function(){
                    jQuery("body").removeClass("side-right");
                });
            }
            
            // ------------------------------------------------------------------------------ //
            // Navbar Fixed
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("no-background")){
                jQuery(window).on("scroll", function(){
                    var scrollTop = jQuery(window).scrollTop();
                    if(scrollTop >34){
                        jQuery(".navbar-fixed").removeClass("no-background");
                    }else {
                        jQuery(".navbar-fixed").addClass("no-background");
                    }
                });
            }
            
            // ------------------------------------------------------------------------------ //
            // Navbar Fixed
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("navbar-transparent")){
                jQuery(window).on("scroll", function(){
                    var scrollTop = jQuery(window).scrollTop();
                    if(scrollTop >34){
                        jQuery(".navbar-fixed").removeClass("navbar-transparent");
                    }else {
                        jQuery(".navbar-fixed").addClass("navbar-transparent");
                    }
                });
            }
            
            // ------------------------------------------------------------------------------ //
            // Button Cart
            // ------------------------------------------------------------------------------ //
            jQuery(".btn-cart").on("click", function(e){
                e.stopPropagation();
            });
            
            // ------------------------------------------------------------------------------ //
            // Toggle Search
            // ------------------------------------------------------------------------------ //
            jQuery("nav.navbar.bootsnav .attr-nav").each(function(){  
                jQuery("li.search > a", this).on("click", function(e){
                    e.preventDefault();
                    jQuery(".top-search").slideToggle();
                });
            });
            jQuery(".input-group-addon.close-search").on("click", function(){
                jQuery(".top-search").slideUp();
            });
            
            // ------------------------------------------------------------------------------ //
            // Toggle Side Menu
            // ------------------------------------------------------------------------------ //
            jQuery("nav.navbar.bootsnav .attr-nav").each(function(){  
                jQuery("li.side-menu > a", this).on("click", function(e){
                    e.preventDefault();
                    jQuery("nav.navbar.bootsnav > .side").toggleClass("on");
                    jQuery("body").toggleClass("on-side");
                });
            });
            jQuery(".side .close-side").on("click", function(e){
                e.preventDefault();
                jQuery("nav.navbar.bootsnav > .side").removeClass("on");
                jQuery("body").removeClass("on-side");
            });  
            
            
            
            // ------------------------------------------------------------------------------ //
            // Wrapper
            // ------------------------------------------------------------------------------ //
            jQuery("body").wrapInner( "<div class='wrapper'></div>");
        }, 
        

        // ------------------------------------------------------------------------------ //
        // Change dropdown to hover on dekstop
        // ------------------------------------------------------------------------------ //
        hoverDropdown : function(){
            var getNav = jQuery("nav.navbar.bootsnav"),
                getWindow = jQuery(window).width(),
                getHeight = jQuery(window).height(),
                getIn = getNav.find("ul.nav").data("in"),
                getOut = getNav.find("ul.nav").data("out");
            
            if( getWindow < 991 ){
                
                // Height of scroll navigation sidebar
                jQuery(".scroller").css("height", "auto");
                
                // Disable mouseenter event
                jQuery("nav.navbar.bootsnav ul.nav").find("li.dropdown").off("mouseenter");
                jQuery("nav.navbar.bootsnav ul.nav").find("li.dropdown").off("mouseleave");
                jQuery("nav.navbar.bootsnav ul.nav").find(".title").off("mouseenter"); 
                jQuery("nav.navbar.bootsnav ul.nav").off("mouseleave");    
                jQuery(".navbar-collapse").removeClass("animated");
                
                // Enable click event
                jQuery("nav.navbar.bootsnav ul.nav").each(function(){
                    jQuery(".dropdown-menu", this).addClass("animated");
                    jQuery(".dropdown-menu", this).removeClass(getOut);
                    
                    // Dropdown Fade Toggle
                    jQuery("a.dropdown-toggle", this).off('click');
                    jQuery("a.dropdown-toggle", this).on('click', function (e) {
                        e.stopPropagation();
                        jQuery(this).closest("li.dropdown").find(".dropdown-menu").first().stop().fadeToggle().toggleClass(getIn);
                        jQuery(this).closest("li.dropdown").first().toggleClass("on");                        
                        return false;
                    });   
                    
                    // Hidden dropdown action
                    jQuery('li.dropdown', this).each(function () {
                        jQuery(this).find(".dropdown-menu").stop().fadeOut();
                        jQuery(this).on('hidden.bs.dropdown', function () {
                            jQuery(this).find(".dropdown-menu").stop().fadeOut();
                        });
                        return false;
                    });

                    // Megamenu style
                    jQuery(".megamenu-fw", this).each(function(){
                        jQuery(".col-menu", this).each(function(){
                            jQuery(".content", this).addClass("animated");
                            jQuery(".content", this).stop().fadeOut();
                            jQuery(".title", this).off("click");
                            jQuery(".title", this).on("click", function(){
                                jQuery(this).closest(".col-menu").find(".content").stop().fadeToggle().addClass(getIn);
                                jQuery(this).closest(".col-menu").toggleClass("on");
                                return false;
                            });

                            jQuery(".content", this).on("click", function(e){
                                e.stopPropagation();
                            });
                        });
                    });  
                }); 
                
                // Hidden dropdown
                var cleanOpen = function(){
                    jQuery('li.dropdown', this).removeClass("on");
                    jQuery(".dropdown-menu", this).stop().fadeOut();
                    jQuery(".dropdown-menu", this).removeClass(getIn);
                    jQuery(".col-menu", this).removeClass("on");
                    jQuery(".col-menu .content", this).stop().fadeOut();
                    jQuery(".col-menu .content", this).removeClass(getIn);
                }
                
                // Hidden om mouse leave
                jQuery("nav.navbar.bootsnav").on("mouseleave", function(){
                    cleanOpen();
                });
                
                // Enable click atribute navigation
                jQuery("nav.navbar.bootsnav .attr-nav").each(function(){  
                    jQuery(".dropdown-menu", this).removeClass("animated");
                    jQuery("li.dropdown", this).off("mouseenter");
                    jQuery("li.dropdown", this).off("mouseleave");                    
                    jQuery("a.dropdown-toggle", this).off('click');
                    jQuery("a.dropdown-toggle", this).on('click', function (e) {
                        e.stopPropagation();
                        jQuery(this).closest("li.dropdown").find(".dropdown-menu").first().stop().fadeToggle();
                        jQuery(".navbar-toggle").each(function(){
                            jQuery(".fa", this).removeClass("fa-times");
                            jQuery(".fa", this).addClass("fa-bars");
                            jQuery(".navbar-collapse").removeClass("in");
                            jQuery(".navbar-collapse").removeClass("on");
                        });
                    });
                    
                    jQuery(this).on("mouseleave", function(){
                        jQuery(".dropdown-menu", this).stop().fadeOut();
                        jQuery("li.dropdown", this).removeClass("on");
                        return false;
                    });
                });
                
                // Toggle Bars
                jQuery(".navbar-toggle").each(function(){
                    jQuery(this).off("click");
                    jQuery(this).on("click", function(){
                        jQuery(".fa", this).toggleClass("fa-bars");
                        jQuery(".fa", this).toggleClass("fa-times");
                        cleanOpen();
                    });
                });

            }else if( getWindow > 991 ){
                // Height of scroll navigation sidebar
                jQuery(".scroller").css("height", getHeight + "px");
                
                // Navbar Sidebar
                if( getNav.hasClass("navbar-sidebar")){
                    // Hover effect Sidebar Menu
                    jQuery("nav.navbar.bootsnav ul.nav").each(function(){  
                        jQuery("a.dropdown-toggle", this).off('click');
                        jQuery("a.dropdown-toggle", this).on('click', function (e) {
                            e.stopPropagation();
                        }); 

                        jQuery(".dropdown-menu", this).addClass("animated");
                        jQuery("li.dropdown", this).on("mouseenter", function(){
                            jQuery(".dropdown-menu", this).eq(0).removeClass(getOut);
                            jQuery(".dropdown-menu", this).eq(0).stop().fadeIn().addClass(getIn);
                            jQuery(this).addClass("on");
                            return false;
                        });
                        
                        jQuery(".col-menu").each(function(){
                            jQuery(".content", this).addClass("animated");
                            jQuery(".title", this).on("mouseenter", function(){
                                jQuery(this).closest(".col-menu").find(".content").stop().fadeIn().addClass(getIn);
                                jQuery(this).closest(".col-menu").addClass("on");
                                return false;
                            });
                        });
                        
                        jQuery(this).on("mouseleave", function(){
                            jQuery(".dropdown-menu", this).stop().removeClass(getIn);
                            jQuery(".dropdown-menu", this).stop().addClass(getOut).fadeOut();
                            jQuery(".col-menu", this).find(".content").stop().fadeOut().removeClass(getIn);
                            jQuery(".col-menu", this).removeClass("on");
                            jQuery("li.dropdown", this).removeClass("on");
                            return false;
                        });
                    }); 
                }else{
                    // Hover effect Default Menu
                    jQuery("nav.navbar.bootsnav ul.nav").each(function(){  
                        jQuery("a.dropdown-toggle", this).off('click');
                        jQuery("a.dropdown-toggle", this).on('click', function (e) {
                            e.stopPropagation();
                        }); 

                        jQuery(".megamenu-fw", this).each(function(){
                            jQuery(".title", this).off("click");
                            jQuery("a.dropdown-toggle", this).off("click");
                            jQuery(".content").removeClass("animated");
                        });

                        jQuery(".dropdown-menu", this).addClass("animated");
                        jQuery("li.dropdown", this).on("mouseenter", function(){
                            jQuery(".dropdown-menu", this).eq(0).removeClass(getOut);
                            jQuery(".dropdown-menu", this).eq(0).stop().fadeIn().addClass(getIn);
                            jQuery(this).addClass("on");
                            return false;
                        });

                        jQuery("li.dropdown", this).on("mouseleave", function(){
                            jQuery(".dropdown-menu", this).eq(0).removeClass(getIn);
                            jQuery(".dropdown-menu", this).eq(0).stop().fadeOut().addClass(getOut);
                            jQuery(this).removeClass("on");
                        });

                        jQuery(this).on("mouseleave", function(){
                            jQuery(".dropdown-menu", this).removeClass(getIn);
                            jQuery(".dropdown-menu", this).eq(0).stop().fadeOut().addClass(getOut);
                            jQuery("li.dropdown", this).removeClass("on");
                            return false;
                        });
                    });
                }
                
                // ------------------------------------------------------------------------------ //
                // Hover effect Atribute Navigation
                // ------------------------------------------------------------------------------ //
                jQuery("nav.navbar.bootsnav .attr-nav").each(function(){                      
                    jQuery("a.dropdown-toggle", this).off('click');
                    jQuery("a.dropdown-toggle", this).on('click', function (e) {
                        e.stopPropagation();
                    }); 
                    
                    jQuery(".dropdown-menu", this).addClass("animated");
                    jQuery("li.dropdown", this).on("mouseenter", function(){
                        jQuery(".dropdown-menu", this).eq(0).removeClass(getOut);
                        jQuery(".dropdown-menu", this).eq(0).stop().fadeIn().addClass(getIn);
                        jQuery(this).addClass("on");
                        return false;
                    });

                    jQuery("li.dropdown", this).on("mouseleave", function(){
                        jQuery(".dropdown-menu", this).eq(0).removeClass(getIn);
                        jQuery(".dropdown-menu", this).eq(0).stop().fadeOut().addClass(getOut);
                        jQuery(this).removeClass("on");
                    });

                    jQuery(this).on("mouseleave", function(){
                        jQuery(".dropdown-menu", this).removeClass(getIn);
                        jQuery(".dropdown-menu", this).eq(0).stop().fadeOut().addClass(getOut);
                        jQuery("li.dropdown", this).removeClass("on");
                        return false;
                    });
                });
            }
            
            // ------------------------------------------------------------------------------ //
            // Menu Fullscreen
            // ------------------------------------------------------------------------------ //
            if( getNav.hasClass("navbar-full")){
                var windowHeight = jQuery(window).height(),
                    windowWidth =  jQuery(window).width();

                jQuery(".nav-full").css("height", windowHeight + "px");
                jQuery(".wrap-full-menu").css("height", windowHeight + "px");
                jQuery(".wrap-full-menu").css("width", windowWidth + "px");
                
                jQuery(".navbar-collapse").addClass("animated");
                jQuery(".navbar-toggle").each(function(){
                    var getId = jQuery(this).data("target");
                    jQuery(this).off("click");
                    jQuery(this).on("click", function(e){
                        e.preventDefault();
                        jQuery(getId).removeClass(getOut);
                        jQuery(getId).addClass("in");
                        jQuery(getId).addClass(getIn);
                        return false;
                    });
                    
                    jQuery("li.close-full-menu").on("click", function(e){
                        e.preventDefault();
                        jQuery(getId).addClass(getOut);
                        setTimeout(function(){
                            jQuery(getId).removeClass("in");
                            jQuery(getId).removeClass(getIn);
                        }, 500);
                        return false;
                    });
                });
            }
        },
        
        // ------------------------------------------------------------------------------ //
        // Navbar Sticky
        // ------------------------------------------------------------------------------ //
        navbarSticky : function(){  
            var getNav = jQuery("nav.navbar.bootsnav"),
                navSticky = getNav.hasClass("navbar-sticky");
            
            if( navSticky ){
                
                // Set Height Navigation
                var getHeight = getNav.height();             
                jQuery(".wrap-sticky").height(getHeight);
                
                // Windown on scroll
                var getOffset = jQuery(".wrap-sticky").offset().top;
                jQuery(window).on("scroll", function(){  
                    var scrollTop = jQuery(window).scrollTop();
                    if(scrollTop > getOffset){
                        getNav.addClass("sticked");
                    }else {
                        getNav.removeClass("sticked");
                    }
                });
            }   
        },
        
        // ------------------------------------------------------------------------------ //
        // Navbar Scrollspy
        // ------------------------------------------------------------------------------ //
        navbarScrollspy : function(){ 
            var navScrollSpy = jQuery(".navbar-scrollspy"),
                jQuerybody   = jQuery('body'), 
                getNav = jQuery('nav.navbar.bootsnav'),
                offset  = getNav.outerHeight();
            
            if( navScrollSpy.length ){
                jQuerybody.scrollspy({target: '.navbar', offset: offset });
                
                // Animation Scrollspy
                jQuery('.scroll').on('click', function(event) {
                    event.preventDefault();

                    // Active link
                    jQuery('.scroll').removeClass("active");
                    jQuery(this).addClass("active");

                    // Remove navbar collapse
                    jQuery(".navbar-collapse").removeClass("in");

                    // Toggle Bars
                    jQuery(".navbar-toggle").each(function(){
                        jQuery(".fa", this).removeClass("fa-times");
                        jQuery(".fa", this).addClass("fa-bars");
                    });

                    // Scroll
                    var scrollTop = jQuery(window).scrollTop(),
                        jQueryanchor = jQuery(this).find('a'),
                        jQuerysection = jQuery(jQueryanchor.attr('href')).offset().top,
                        jQuerywindow = jQuery(window).width(),
                        jQueryminusDesktop = getNav.data("minus-value-desktop"),
                        jQueryminusMobile = getNav.data("minus-value-mobile"),
                        jQueryspeed = getNav.data("speed");
                    
                    if( jQuerywindow > 992 ){
                        var jQueryposition = jQuerysection - jQueryminusDesktop;
                    }else{
                        var jQueryposition = jQuerysection - jQueryminusMobile;
                    }             
                        
                    jQuery('html, body').stop().animate({
                        scrollTop: jQueryposition
                    }, jQueryspeed);
                });
                
                // Activate Navigation
                var fixSpy = function() {
                    var data = jQuerybody.data('bs.scrollspy');
                    if (data) {
                        offset = getNav.outerHeight();
                        data.options.offset = offset;
                        jQuerybody.data('bs.scrollspy', data);
                        jQuerybody.scrollspy('refresh');
                    }
                }
                
                // Activate Navigation on resize
                var resizeTimer;
                jQuery(window).on('resize', function() {
                    clearTimeout(resizeTimer);
                    var resizeTimer = setTimeout(fixSpy, 200);
                });
            }
        }
    };
    
    // Initialize
    jQuery(document).ready(function(){
        bootsnav.initialize();
    });
    
    // Reset on resize
    jQuery(window).on("resize", function(){   
        bootsnav.hoverDropdown();
        setTimeout(function(){
            bootsnav.navbarSticky();
        }, 500);
        
        // Toggle Bars
        jQuery(".navbar-toggle").each(function(){
            jQuery(".fa", this).removeClass("fa-times");
            jQuery(".fa", this).addClass("fa-bars");
            jQuery(this).removeClass("fixed");
        });        
        jQuery(".navbar-collapse").removeClass("in");
        jQuery(".navbar-collapse").removeClass("on");
        jQuery(".navbar-collapse").removeClass("bounceIn");      
    });
    
}(jQuery));

