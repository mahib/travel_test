<?php
function travel_integrateWithVC() {
						
//****** ====================================		
// Revulation Slider Area
//******* =========================================

		vc_map(array(
			'name'			=> esc_html__('Revolution Slider', 'travel'),
			'base'			=> 'slider_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
								
						array(
							'param_name'	=> 'content',
							'type'			=> 'textarea_html',
							'heading'		=> esc_html__('Rev Slider Shortcode', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
					),
				)
			);
		
//***** =====================================		
// About Us Section
//******* ================================

		vc_map(array(
			'name'			=> esc_html__('About Us', 'travel'),
			'base'			=> 'about_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
			
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Image', 'travel'),
							'param_name' 	=> 'abt_sec_img'
						),
						array(
							'param_name'	=> 'abt_title',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('About Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
						array(
							'param_name'	=> 'abt_title_color',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('About Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
						array(
							'param_name'	=> 'abt_desc',
							'type'			=> 'textarea',
							'heading'		=> esc_html__('About Description', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'abt_desc_color',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('About Description', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),									
// -------------------------------------------
// params group
// ---------------------------------------------
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Group Items', 'travel'),
							'param_name' 	=> 'about',
							'group'			=> esc_html__('About Items', 'travel'),
						// Note params is mapped inside param-group:
							'params' 			=> array(
							
								array(
										'type' 			=> 'attach_image',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Image', 'travel'),
										'param_name' 	=> 'abt_grp_img'
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Item Title', 'travel'),
										'param_name' 	=> 'abt_grp_title'
								),								
								array(
									'param_name'	=> 'abt_title_grp_color',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('About Grp Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textarea',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Description', 'travel'),
										'param_name' 	=> 'abt_grp_desc'
								),
								array(
									'param_name'	=> 'abt_desc_grp_color',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('About Grp Description Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),
							),
						),
					),
				));	
								
//***** =====================================		
// PACKAGES Section
//******* ================================

		vc_map(array(
			'name'			=> esc_html__('PACKAGES', 'travel'),
			'base'			=> 'packages_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
			
						array(
							'param_name'	=> 'pack_title',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('Packages Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'pack_title_color',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
						array(
							'param_name'	=> 'pack_desc',
							'type'			=> 'textarea',
							'heading'		=> esc_html__('Package Description', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'pack_desc_color',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Description Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Image', 'travel'),
							'param_name' 	=> 'pack_sec_img'
						),
// -------------------------------------------
// params group
// ---------------------------------------------
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Group Items', 'travel'),
							'param_name' 	=> 'packages',
							'group'			=> esc_html__('Package Items', 'travel'),
						// Note params is mapped inside param-group:
							'params' 			=> array(
							
								array(
										'type' 			=> 'attach_image',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Image', 'travel'),
										'param_name' 	=> 'pack_grp_img'
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Location Title', 'travel'),
										'param_name' 	=> 'pack_grp_loc_title'
								),								
								array(
									'param_name'	=> 'pack_grp_loc_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Location Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Location Value', 'travel'),
										'param_name' 	=> 'pack_grp_loc_value'
								),								
								array(
									'param_name'	=> 'pack_grp_loc_value_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Location Value Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Duration Title', 'travel'),
										'param_name' 	=> 'pack_grp_dur_title'
								),								
								array(
									'param_name'	=> 'pack_grp_dur_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Duration Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Duration Value', 'travel'),
										'param_name' 	=> 'pack_grp_dur_value'
								),								
								array(
									'param_name'	=> 'pack_grp_dur_value_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Duration Value Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Seat Title', 'travel'),
										'param_name' 	=> 'pack_grp_seat_title'
								),								
								array(
									'param_name'	=> 'pack_grp_seat_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Seat Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Seat Value', 'travel'),
										'param_name' 	=> 'pack_grp_seat_value'
								),								
								array(
									'param_name'	=> 'pack_grp_seat_value_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Seat Value Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Price Title', 'travel'),
										'param_name' 	=> 'pack_grp_price_title'
								),								
								array(
									'param_name'	=> 'pack_grp_price_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Price Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Price Value', 'travel'),
										'param_name' 	=> 'pack_grp_price_value'
								),								
								array(
									'param_name'	=> 'pack_grp_price_value_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Price Value Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Date Title', 'travel'),
										'param_name' 	=> 'pack_grp_day_title'
								),								
								array(
									'param_name'	=> 'pack_grp_date_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Date Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Date Value', 'travel'),
										'param_name' 	=> 'pack_grp_day_value'
								),								
								array(
									'param_name'	=> 'pack_grp_date_value_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Date Value Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Button Title', 'travel'),
										'param_name' 	=> 'pack_grp_button_title'
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Button Link', 'travel'),
										'param_name' 	=> 'pack_grp_button_link'
								),								
							),
						),
					),
				));
								
//***** =====================================		
// Counter Section
//******* ================================

		vc_map(array(
			'name'			=> esc_html__('Counter Section', 'travel'),
			'base'			=> 'counter_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
// -------------------------------------------
// params group
// ---------------------------------------------
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Group Items', 'travel'),
							'param_name' 	=> 'counter',
							'group'			=> esc_html__('Counter Items', 'travel'),
						// Note params is mapped inside param-group:
							'params' 			=> array(
							
								array(
										'type' 			=> 'attach_image',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Image', 'travel'),
										'param_name' 	=> 'count_grp_img'
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Item Text', 'travel'),
										'param_name' 	=> 'count_grp_text'
								),								
								array(
									'param_name'	=> 'count_grp_text_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Count Grp Text Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Item Title', 'travel'),
										'param_name' 	=> 'count_grp_title'
								),
								array(
									'param_name'	=> 'count_grp_title_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Count Grp Title Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),
							),
						),
					),
				));
								
//****** ====================================		
//  Book Now Section
//******* =========================================

		vc_map(array(
			'name'			=> esc_html__('Book Now', 'travel'),
			'base'			=> 'booking_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
	
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('BG Image', 'travel'),
							'param_name' 	=> 'book_now_img'
						),					
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Icon Image', 'travel'),
							'param_name' 	=> 'book_icn_img'
						),						
						array(
							'type' 			=> 'textfield',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Book Title', 'travel'),
							'param_name' 	=> 'book_now_title'
						),						
						array(
							'param_name'	=> 'book_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Book Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),								
						array(
							'type' 			=> 'textfield',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Sub Title', 'travel'),
							'param_name' 	=> 'book_sub_title'
						),						
						array(
							'param_name'	=> 'book_sub_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Book Sub Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'content',
							'type'			=> 'textarea_html',
							'heading'		=> esc_html__('Contact Shortcode', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
					),
				)
			);
						
//****** ====================================		
//  Video Tour Section
//******* =========================================

		vc_map(array(
			'name'			=> esc_html__('Video Tour', 'travel'),
			'base'			=> 'tour_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
	
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('BG Image', 'travel'),
							'param_name' 	=> 'tour_bg_img'
						),						
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Video Image', 'travel'),
							'param_name' 	=> 'video_poster'
						),						
						array(
							'type' 			=> 'textfield',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Tour Title', 'travel'),
							'param_name' 	=> 'tour_title'
						),						
						array(
							'param_name'	=> 'vid_tour_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Tour Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),								
						array(
							'type' 			=> 'textfield',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Break Title', 'travel'),
							'param_name' 	=> 'tour_break_title'
						),						
						array(
							'type' 			=> 'textarea',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Tour Description', 'travel'),
							'param_name' 	=> 'tour_description'
						),						
						array(
							'param_name'	=> 'vid_tour_description_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Tour Description Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),
						//video-------------------
						 array(
							'type' 			=> 'textfield',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Video Link', 'travel'),
							'param_name' 	=> 'video_link'
						),						
					array(
							'param_name'	=> 'content',
							'type'			=> 'textarea_raw_html',
							'heading'		=> esc_html__('Contact Shortcode', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'grptttt_dropdown',
							'type'			=> 'dropdown',
							'heading'		=> esc_html__('Dropdown', 'travel'),
							'value'			=> array(
								'Link' 	=> '__link',
								'Embed' 	=> '__embed',
								
							), 
						),
					),
				)
			);			
			
//***** =====================================		
// 		Testimonial Section
//******* ================================

		vc_map(array(
			'name'			=> esc_html__('Testimonial', 'travel'),
			'base'			=> 'testimonial_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
// -------------------------------------------
// params group
// ---------------------------------------------				
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Testimonial Items', 'travel'),
							'param_name' 	=> 'testimonial',
							'group'			=> esc_html__('Testimonial', 'travel'),
						// Note params is mapped inside param-group:
							'params' 			=> array(
								
								array(
										'type' 			=> 'attach_image',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Image', 'travel'),
										'param_name' 	=> 'test_grp_img'
								),
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Name', 'travel'),
										'param_name' 	=> 'test_grp_name'
								),								
								array(
									'param_name'	=> 'test_grp_name_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Testi Name Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Designation', 'travel'),
										'param_name' 	=> 'test_grp_desig'
								),								
								array(
									'param_name'	=> 'test_grp_desig_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Testi Designation Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textarea',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Description', 'travel'),
										'param_name' 	=> 'test_grp_desc'
								),
								array(
									'param_name'	=> 'test_grp_desc_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Testi Description Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
									'param_name'	=> 'grp_dropdown',
									'type'			=> 'dropdown',
									'heading'		=> esc_html__('Dropdown', 'travel'),
									'value'			=> array(
										'1' 	=> '1',
										'1.5' 	=> '1.5',
										'2' 	=> '2',
										'2.5' 	=> '2.5',
										'3' 	=> '3',
										'3.5' 	=> '3.5',
										'4' 	=> '4',
										'4.5' 	=> '4.5',
										'5' 	=> '5',
									),									
								),
							),
						),	
					),
				)
			);
					
//****** ====================================		
// Latest Blog Area
//******* =========================================

		vc_map(array(
			'name'			=> esc_html__('Latest Blog', 'travel'),
			'base'			=> 'blog_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
	
						array(
							'type' 			=> 'attach_image',
							'value' 		=> esc_html__('', 'travel'),
							'heading' 		=> esc_html__('Image', 'travel'),
							'param_name' 	=> 'blog_bg'
						),							
						array(
							'param_name'	=> 'title',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'blog_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Blog Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'descrip',
							'type'			=> 'textarea',
							'heading'		=> esc_html__('Description', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'blog_desc_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Blog Description Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'page_numbers',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('Select Page', 'travel'),
							'value'			=> esc_html__('', 'travel'),
						),
					),
				)
			);			
			
//****** ====================================		
// Contact Us Section
//******* =========================================

		vc_map(array(
			'name'			=> esc_html__('Contact Section', 'travel'),
			'base'			=> 'contact_section',
			'category'		=> esc_html__('Travel', 'travel'),
			'Description'	=> esc_html__('All service we Manage', 'travel'),
			'icon'			=> get_template_directory_uri(). '/assets/images/travel/e.jpg',
			'params'		=> array(
								
						array(
							'param_name'	=> 'contact_title',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'contact_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Contact Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'con_sub_title',
							'type'			=> 'textfield',
							'heading'		=> esc_html__('Sub Title', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'con_sub_title_clr',
							'type'			=> 'colorpicker',
							'heading'		=> esc_html__('Contact Sub Title Color', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
						array(
							'param_name'	=> 'content',
							'type'			=> 'textarea_html',
							'heading'		=> esc_html__('Contact Shortcode', 'travel'),
							'value'			=> esc_html__('', 'travel')
						),						
// -------------------------------------------
// params group
// ---------------------------------------------				
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Contact Address', 'travel'),
							'param_name' 	=> 'contact',
							'group'			=> esc_html__('Contact', 'travel'),
						// Note params is mapped inside param-group:
							'params' 			=> array(
								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Address', 'travel'),
										'param_name' 	=> 'con_grp_add'
								),								
								array(
									'param_name'	=> 'con_grp_add_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Address Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),								
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Address OPT 1', 'travel'),
										'param_name' 	=> 'con_grp_opt1'
								),								
								array(
									'param_name'	=> 'con_grp_opt1_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Address OPT 1 Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),						
								array(
										'type' 			=> 'textfield',
										'value' 		=> esc_html__('', 'travel'),
										'heading' 		=> esc_html__('Address OPT 2', 'travel'),
										'param_name' 	=> 'con_grp_opt2'
								),								
								array(
									'param_name'	=> 'con_grp_opt2_clr',
									'type'			=> 'colorpicker',
									'heading'		=> esc_html__('Address OPT 2 Color', 'travel'),
									'value'			=> esc_html__('', 'travel')
								),
							),
						),
					),
				)
			);	
	}
	add_action( 'vc_before_init', 'travel_integrateWithVC' );
