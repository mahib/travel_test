<?php

/* Travel Theme Custom Color
 */
 
function travel_custom_color_css(){

wp_enqueue_style(
        'travel-customize-color',
        get_template_directory_uri() . '/assets/css/custom_script.css'
    );
	
	    $travel_theme_color = cs_get_customize_option( 'travel_base_color' );
	    $travel_hover_color = cs_get_customize_option( 'travel_hover_color' );
		$custom_css = "
		.main-nav .dropdown-menu, .travel-menu.affix {background-color: {$travel_theme_color};}
		.travel-booking-form .send-btn {background: {$travel_theme_color}; border-color: {$travel_theme_color};}
		.sticky:before { background: {$travel_theme_color};}
		.travel-contact-form .sendbtn {background: {$travel_theme_color};}
		
		.travel-menu.navbar-default .navbar-nav>li>a:hover, .travel-menu.navbar-default .navbar-nav>li>a:focus {color: {$travel_hover_color};}
		.travel-package-single:hover .travel-btn {background: {$travel_hover_color};border-color: {$travel_hover_color};}
		.travel-btn.send-btn:hover {color: {$travel_hover_color}; border-color: {$travel_hover_color};}
		.travel-contact-form .sendbtn:hover {background: {$travel_hover_color};}
		.travel-blog-single .readmore:hover {color:{$travel_hover_color};}
		
		.travel-blog-posts .post .travel-btn:hover{background: {$travel_hover_color};border-color: {$travel_hover_color};}
		.page-numbers.current {border-color: {$travel_theme_color};}
		.travel-pagination.pagination a.page-numbers:hover {border-color: {$travel_theme_color};}
		.travel-sidebar-widget .title hr {border-color: {$travel_theme_color};}
		.tagcloud a:hover {background: {$travel_hover_color};border-color: {$travel_hover_color};}
		
		.travel-blog-author-profiles a i:hover {color: {$travel_hover_color};}
		.travel-blog-author-profiles hr {border-color: {$travel_theme_color};}
		.comments-area .travel-btn {border-color: {$travel_theme_color};background: {$travel_theme_color};}
		#blog-top .breadcrumb, #blog-top .breadcrumb a {color: {$travel_theme_color};}
		.travel-blog-top-content h1{color: {$travel_theme_color};}
		
		.main-nav .dropdown-menu li a:hover, .main-nav .dropdown-menu li a:focus{color: {$travel_hover_color};}
		#travelScrollUp i:hover {color: {$travel_hover_color};}
	
		";
		
	wp_add_inline_style( 'travel-customize-color', $custom_css );
}
add_action('wp_enqueue_scripts','travel_custom_color_css');