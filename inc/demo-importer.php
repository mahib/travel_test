<?php

include_once( ABSPATH . 'wp-admin/includes/plugin.php');
if( is_plugin_active( 'dt_demo_importer/init.php')){
 
	$settings      = array(
	  'menu_parent' => 'tools.php',
	  'menu_title'  => __('Travel Demo Importer', 'travel'),
	  'menu_type'   => 'menu',
	  'menu_slug'   => 'dt_demo_importer',
	);
	$options        = array(
		'demo-1' => array(
		  'title'         => __('Demo 1', 'travel'),
		  'preview_url'   => 'https://www.google.com/',
		  'front_page'    => 'Home',
		  'blog_page'     => 'Blog',
		  'menus'         => array(
				'header_menu' => __('Header Menu','travel'), // Menu Location and Title
				'footer_menu' => __('Footer Menu','travel'),
			)
		),
	);
	DT_Demo_Importer::instance( $settings, $options );

 }