<!DOCTYPE HTML>
<html <?php language_attributes(); ?> lang="en-US">
<head>
	<!-- META DATA -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php wp_title(); ?></title>
	<!-- favicon -->
	<?php if(!(function_exists('has_site_icon') & has_site_icon())){ ?>
			<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url(cs_get_option('travel_fav_icon'));?>" >
	<?php   } ?>

	<?php wp_head();?>
</head>
<body <?php body_class('travel-template'); ?>>
	<!-- Pre-loader Start -->	
	<div id="spinningSquaresG1">
		<div id="spinningSquaresG2">
			<div id="cssload-loader">
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
				<div class="cssload-dot"></div>
			</div>
		</div>
	</div> 
	<!-- Pre-loader End -->	
	<!-- Start Header -->
  	<header id="travel-header" class="travel-header">
		<nav id="travel-menu" class="main-nav travel-menu navbar navbar-default navbar-fixed-top" data-spy="affix" data-offset-top="200">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
				   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex1-collapse" aria-expanded="false">
				      <span class="sr-only"><?php __('Toggle navigation','travel');?></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				   </button>
				   <a class="navbar-brand page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				      <div class="logo">
						  <?php
								if(cs_get_option('travel_logo')){?>
									<img src="<?php echo esc_url(wp_get_attachment_image_src( cs_get_option('travel_logo'), 'full') [0]);?>" alt="<?php _e('TRAVEL','travel');?>" />
								<?php }
							?>
				      </div>
				   </a>
				</div>
				<!-- Menu -->
					<?php
						if(has_nav_menu('header_menu')){
						wp_nav_menu( array(
							'menu'              => 'header_menu',
							'theme_location'    => 'header_menu',
							'depth'             => 10,
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'container_id'      => 'navbar-ex1-collapse',
							'menu_class'        => 'nav navbar-nav navbar-right',
							'fallback_cb'       => 'travel_WP_Bootstrap_Navwalker::fallback',
							'walker'            => new travel_WP_Bootstrap_Navwalker()
						));
						} else { ?>
								<ul class="nav navbar-nav set_menu">
									<li><a href="<?php echo admin_url('nav-menus.php'); ?>"><?php echo esc_html__('Set Up Your Menu','travel');?></a></li>
								</ul>
						<?php }
					?>
											
			</div><!-- /.container -->
		</nav>
  	</header>
  	<!-- End Header -->