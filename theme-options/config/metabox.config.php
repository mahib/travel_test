<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options      = array();

// -----------------------------------------
// Post Metabox Options For Aside          -
// -----------------------------------------
$options[]    = array(
  'id'        => '_aside_posts',
  'title'     => 'Post Format: Aside',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_aside',
      'fields' => array(

		array(
		  'id'            => 'aside_title',
		  'type'          => 'text',
		  'title'          => 'Title if any',
		  'attributes'    => array(
			'data-format' => 'aside',
		  ),
		),
		array(
          'id'    => 'aside_id',
          'type'  => 'textarea',
          'title' => 'Aside Embeded Code',
          'sanitize' => 'disabled',
        ),
	
      ),
    ),

  ),
);

// -----------------------------------------
// Post Metabox Options For Image          -
// -----------------------------------------
$options[]    = array(
  'id'        => '_image_posts',
  'title'     => 'Post Format: Image',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_image',
      'fields' => array(
	
		array(
		  'id'            => 'image',
		  'type'          => 'upload',
		  'title'         => 'Select or Upload Image',
		  'settings'      => array(
		   'upload_type'  => 'image',
		   'button_title' => 'Upload Image',
		   'frame_title'  => 'Select an image',
		   'insert_title' => 'Use this image',
		  ),
		  'attributes'    => array(
			'data-format' => 'image',
		  ),
		),
		
      ),
    ),

  ),
);

// -----------------------------------------
// Post Metabox Options For Video          -
// -----------------------------------------
$options[]    = array(
  'id'        => '_video_posts',
  'title'     => 'Post Format: Video',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_video',
      'fields' => array(

		array(
		  'id'            => 'video_title',
		  'type'          => 'text',
		  'title'          => 'Title if any',
		  'attributes'    => array(
			'data-format' => 'video',
		  ),
		),
		array(
          'id'    => 'video_id',
          'type'  => 'textarea',
          'title' => 'Video Embeded Code',
          'sanitize' => 'disabled',
        ),
	
      ),
    ),

  ),
);


// -----------------------------------------
// Post Metabox Options For Quote          -
// -----------------------------------------
$options[]    = array(
  'id'        => '_quote_posts',
  'title'     => 'Post Format: Quote',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_quote',
      'fields' => array(

		array(
		  'id'    => 'quote_textarea',
		  'type'  => 'textarea',
		  'title' => 'Quote',
		),
	
		array(
		  'id'    => 'quote', 
		  'type'  => 'text',
		  'title' => 'Author',
		  'attributes'    => array(
			'data-format' => 'quote',
		  ),
		),
		array(
			'id'      => 'quote_font_clr',
			'type'    => 'color_picker',
			'title'   => 'Font Color',
			'default' => '#373737',
			'rgba'    => true,
			),
		array(
			'id'      => 'quote_background',
			'type'    => 'color_picker',
			'title'   => 'Background Color',
			'default' => '#dddddd',
			'rgba'    => true,
			),
		array(
		  'id'    => 'quote_text', 
		  'type'  => 'text',
		  'title' => 'Author URL',
		),
	
      ),
    ),

  ),
);

// -----------------------------------------
// Post Metabox Options For Link     -
// -----------------------------------------
$options[]    = array(
  'id'        => '_link_posts',
  'title'     => 'Post Format: Link',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_link',
      'fields' => array(

		array(
		  'id'            => 'link',
		  'type'          => 'text',
		  'title'         => 'Link',
		  'attributes'    => array(
			'data-format' => 'link',
		  ),
		),
		array(
		  'id'            => 'link_text',
		  'type'          => 'text',
		  'title'         => 'Text',
		),
		array(
			'id'      => '',
			'type'    => 'color_picker',
			'title'   => 'Font Color',
			'default' => '#373737',
			'rgba'    => true,
		),
		array(
			'id'      => 'link_background',
			'type'    => 'color_picker',
			'title'   => 'Background Color',
			'default' => '#dddddd',
			'rgba'    => true,
		),
      ),
    ),

  ),
);

// -----------------------------------------
// Post Metabox Options For Gallery  -
// -----------------------------------------
 $options[]    = array(
  'id'        => '_gallery_posts',
  'title'     => 'Post Format: Gallery',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_gallery',
      'fields' => array(

		array(
		  'id'            => 'gallery',
		  'type'          => 'gallery',
		  'title'         => 'Select or Upload Image',
		  'desc'          => 'Add multiple images in this gallery and change their order',
		  'attributes'    => array(
			'data-format' => 'gallery',
		  ),
		),
      ),
    ),

  ),
); 


// -----------------------------------------
// Post Metabox Options For Audio          -
// -----------------------------------------
$options[]    = array(
  'id'        => '_audio_posts',
  'title'     => 'Poat Format: Audio',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_audio',
      'fields' => array(

		array(
		  'id'            => 'audio_title',
		  'type'          => 'text',
		  'title'          => 'Title',
		  'attributes'    => array(
			'data-format' => 'audio',
		  ),
		),
		array(
		  'id'            => 'audio_id',
		  'type'          => 'textarea',
		  'title'         => 'Audio Embeded Code',
		  'sanitize' => 'disabled',
		),
      ),
    ),

  ),
);

// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------
$options[]    = array(
  'id'        => '_custom_page_options',
  'title'     => 'Custom Page Options',
  'post_type' => 'page',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    // begin: a section
    array(
      'name'  => 'section_1',
      'title' => 'Section 1',
      'icon'  => 'fa fa-cog',

      // begin: fields
      'fields' => array(

        // begin: a field
        array(
          'id'    => 'section_1_text',
          'type'  => 'text',
          'title' => 'Text Field',
        ),
        // end: a field

        array(
          'id'    => 'section_1_textarea',
          'type'  => 'textarea',
          'title' => 'Textarea Field',
        ),
        array(
          'id'    => 'section_1_upload',
          'type'  => 'upload',
          'title' => 'Upload Field',
        ),
        array(
          'id'    => 'section_1_switcher',
          'type'  => 'switcher',
          'title' => 'Switcher Field',
          'label' => 'Yes, Please do it.',
        ),
      ), // end: fields
    ), // end: a section

    // begin: a section
    array(
      'name'  => 'section_2',
      'title' => 'Section 2',
      'icon'  => 'fa fa-tint',
      'fields' => array(

        array(
          'id'      => 'section_2_color_picker_1',
          'type'    => 'color_picker',
          'title'   => 'Color Picker 1',
          'default' => '#2ecc71',
        ),
        array(
          'id'      => 'section_2_color_picker_2',
          'type'    => 'color_picker',
          'title'   => 'Color Picker 2',
          'default' => '#3498db',
		),
        array(
          'id'      => 'section_2_color_picker_3',
          'type'    => 'color_picker',
          'title'   => 'Color Picker 3',
          'default' => '#9b59b6',
        ),
        array(
          'id'      => 'section_2_color_picker_4',
          'type'    => 'color_picker',
          'title'   => 'Color Picker 4',
          'default' => '#34495e',
        ),
        array(
          'id'      => 'section_2_color_picker_5',
          'type'    => 'color_picker',
          'title'   => 'Color Picker 5',
          'default' => '#e67e22',
        ),
      ),
    ),
    // end: a section
  ),
);

// -----------------------------------------
// Page Side Metabox Options               -
// -----------------------------------------
$options[]    = array(
  'id'        => '_custom_page_side_options',
  'title'     => 'Custom Page Side Options',
  'post_type' => 'page',
  'context'   => 'side',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_3',
      'fields' => array(

        array(
          'id'        => 'section_3_image_select',
          'type'      => 'image_select',
          'options'   => array(
            'value-1' => 'http://codestarframework.com/assets/images/placeholder/65x65-2ecc71.gif',
            'value-2' => 'http://codestarframework.com/assets/images/placeholder/65x65-e74c3c.gif',
            'value-3' => 'http://codestarframework.com/assets/images/placeholder/65x65-3498db.gif',
          ),
          'default'   => 'value-2',
        ),
        array(
          'id'            => 'section_3_text',
          'type'          => 'text',
          'attributes'    => array(
            'placeholder' => 'do stuff'
          )
        ),
        array(
          'id'      => 'section_3_switcher',
          'type'    => 'switcher',
          'label'   => 'Are you sure ?',
          'default' => true
        ),
      ),
    ),

  ),
);

// -----------------------------------------
// Post Metabox Options                    -
// -----------------------------------------
$options[]    = array(
  'id'        => '_custom_post_options',
  'title'     => 'Custom Post Options',
  'post_type' => 'post',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_4',
      'fields' => array(

        array(
          'id'    => 'section_4_text',
          'type'  => 'text',
          'title' => 'Text Field',
        ),
        array(
          'id'    => 'section_4_textarea',
          'type'  => 'textarea',
          'title' => 'Textarea Field',
        ),
        array(
          'id'    => 'section_4_upload',
          'type'  => 'upload',
          'title' => 'Upload Field',
        ),
        array(
          'id'    => 'section_4_switcher',
          'type'  => 'switcher',
          'title' => 'Switcher Field',
          'label' => 'Yes, Please do it.',
        ),

      ),
    ),

  ),
);

CSFramework_Metabox::instance( $options );