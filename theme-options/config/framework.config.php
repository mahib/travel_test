<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'Travel Theme Options',
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'travel',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => 'Codestar Framework <small>by Codestar</small>',
);
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();
// ----------------------------------------
// a option section for options header Logo  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'header',
  'title'       => 'Header',
  'icon'        => 'fa fa-star',
  'fields'      => array( 
				array(
					  'type'    => 'heading',
					  'content' => 'Logo Heading',
				),
				array(
				  'id'    => 'travel_fav_icon',
				  'type'  => 'upload',
				  'title' => 'Fav Icon',
				),
				array(
				  'id'    => 'travel_logo',
				  'type'  => 'image',
				  'title' => 'Logo',
				),
			),
		);
// ----------------------------------------
// a option section for options footer section  -
// ----------------------------------------
$options[] = array(
	'name' => 'copy',
	'title' => 'Footer',
	'icon' => 'fa fa-copyright',
	'fields' => array(	
		array(
		  'type'    => 'heading',
		  'content' => 'Copyright Section',
		),
		array(
		  'id'    => 'author_link',
		  'type'  => 'text',
		  'title' => 'Template',
		),	
		array(
		  'id'    => 'author_title',
		  'type'  => 'text',
		  'title' => 'Template',
		),
		array(
		  'id'    => 'reserved_title',
		  'type'  => 'text',
		  'title' => 'Reserved',
		),	
		array(
		  'type'    => 'heading',
		  'content' => 'Social Section',
		),	
		// begin: group fields
		array(
		  'id'              => 'icon_group',
		  'type'            => 'group',
		  'title'           => 'Top Right Field',
		  'button_title'    => 'Add New',
		  'accordion_title' => 'Add New Field',
		  'fields'          => array(
				array(
				  'id'          => 'foo_icon_link',
				  'type'        => 'text',
				  'title'       => 'Icon Link',
				),	
				array(
				  'id'          => 'footer_icon',
				  'type'        => 'icon',
				  'title'       => 'Icon Field',
				),
			),
		),
			
	),
);

// ----------------------------------------
// a option section for Blog top & Newsletter Heading  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'blog-top',
  'title'       => 'Blog Top',
  'icon'        => 'fa fa-newspaper-o',
  'fields'      => array(
  
				array(
					  'type'    => 'heading',
					  'content' => 'Blog Top',
				),
				array(
				  'id'    => 'blog_top_img',
				  'type'  => 'upload',
				  'title' => 'Top Image',
				),
				array(
				  'id'    => 'top_title',
				  'type'  => 'text',
				  'title' => 'Top Title',
				),			
				array(
				  'id'    => 'newsletter_title',
				  'type'  => 'text',
				  'title' => 'Newsletter Title',
				),
			),
		);

// ------------------------------
// backup                       -
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => 'Backup',
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => 'You can save your current options. Download a Backup and Import.',
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

CSFramework::instance( $settings, $options );
