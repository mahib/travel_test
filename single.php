<?php get_header();?>
<!-- post-top start-->
	<section id="blog-top">
	
		<div class="travel-blog-top-content" style="background: url(<?php 
		if(cs_get_option('blog_top_img')) {
			echo esc_url(cs_get_option('blog_top_img'));
		}
		?>)">
			<div class="container">
			   <h1><?php echo esc_html(cs_get_option('top_title'));?></h1>      
			   <ol class="travel-breadcrumb breadcrumb">
				  <?php if (function_exists('travel_custom_breadcrumbs')) travel_custom_breadcrumbs(); ?>
			   </ol>
			</div>      
		 <div class="overlay"></div>
		</div>
   </section>
   <!-- post-top end-->
	
	<!-- post-section start-->
   <div id="post-section" >
		<div class="container">
			<div class="row">
				<?php
					while(have_posts()) : the_post();	
				?>
				<div class="col-md-8 col-sm-8">
					<div class="travel-blog-posts singlepage">
					<?php $sticky = !is_sticky() ? "non-sticky" : 'sticky'; ?>
						<article  id="post-<?php the_ID(); ?>" <?php post_class($sticky); ?>>

							<?php
							$format = array( 'image', 'video', 'gallery', 'aside', 'link', 'audio', 'quote', 'status', 'chat' ); 
							if ( has_post_format( $format) || has_post_thumbnail() ) {?>

								<div class="post-image">
									  <?php get_template_part( 'template-parts/front', 'postformate');?>
								</div>
								<?php }
								?>

							<div class="post-content">
								
								<h2><a href="<?php the_permalink(); ?>"><?php esc_html(the_title());?></a></h2>
								<div class="post-meta">
									<span><?php _e('By: ','travel');?> <?php the_author(); ?></span>
									<span>&nbsp;I&nbsp;  <?php _e('On : ','travel');?> <?php echo esc_html(get_the_time('F j.Y')); ?> </span>
									<span>&nbsp;I&nbsp;  <?php _e('Tagged :','travel');?> <?php the_category(', '); ?>  </span>
								</div>
								<p><?php the_content(); ?></p>
							</div><!-- /.post-content -->							
						</article><!-- /.post -->
						<div class="travel-blog-author">
							<div class="row">
								<div class="col-md-2">
									<div class="thumb">
										<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
									</div>
								</div>
								<div class="col-md-10">
									<div class="info">
										<h4><?php _e('About The Author','travel');?></h4>
										<h5><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php the_author();?></a>
										</h5>
										<p><?php the_author_meta( 'description') ?></p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="travel-blog-author-profiles text-center">
							 <a href="<?php the_author_meta( 'facebook' ); ?>"><i class="fa fa-facebook"></i></a>
							 <a href="<?php the_author_meta( 'gplus' ); ?>"><i class="fa fa-google-plus"></i></a>
							 <a href="<?php the_author_meta( 'twitter' ); ?>"><i class="fa fa-twitter"></i></a>
							 <a href="<?php the_author_meta( 'linkedin' ); ?>"><i class="fa fa-linkedin"></i></a>
							 <a href="<?php the_author_meta( 'pinterest' ); ?>"><i class="fa fa-pinterest-p"></i></a>
								<hr>
						</div>
						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
						?>
					</div><!-- /.blog-posts -->
				</div><!-- /.col-md-8 -->
				<?php endwhile; ?>
				
				<?php get_sidebar(); ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div>
	<!-- post-section end -->

	<!-- subscribe start -->
	<?php if(!empty(cs_get_option('newsletter_title'))){?>
	<section id="subscribe">
		<div class="travel-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2><?php echo esc_html(cs_get_option('newsletter_title'));?></h2>
					</div>
					<div class="col-md-offset-1 col-md-5">
						<?php echo do_shortcode('[newsletter]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php  } ?>
	<!-- subscribe end -->
<?php get_footer();?>