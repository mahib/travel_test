<?php get_header();?>
<!-- post-top start-->
	<section id="blog-top">
		<div class="travel-blog-top-content" style="background: url(<?php 
		if(cs_get_option('blog_top_img')) {
			echo esc_url(cs_get_option('blog_top_img'));
		}
		?>)">	
			<div class="container">
			   <h1><?php echo esc_html(cs_get_option('top_title'));?></h1>      
			   <ol class="travel-breadcrumb breadcrumb">
				  <?php if (function_exists('travel_custom_breadcrumbs')) travel_custom_breadcrumbs(); ?>
			   </ol>
			</div>      
		 <div class="overlay"></div>
		</div>
   </section>
   <!-- post-top end-->
	
	<!-- post-section start-->
   <div id="post-section" >
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="travel-blog-posts ">
					<?php
						if(have_posts()) : while(have_posts()) : the_post();	
					?>
					<?php $sticky = !is_sticky() ? "non-sticky" : "sticky";?>
						<article  id="post-<?php the_ID(); ?>" <?php post_class($sticky); ?>>	
						<?php
						$format = array( 'image', 'video', 'gallery', 'aside', 'link', 'audio', 'quote', 'status', 'chat' ); 
                        if ( has_post_format( $format) || has_post_thumbnail() ) {?>
							<div class="post-image">
								  <?php get_template_part( 'template-parts/front', 'postformate');?>
							</div>
							<?php }
							?>
							<div class="post-content">
								<h2><a href="<?php the_permalink(); ?>" ><?php esc_html(the_title());?></a></h2>
								<div class="post-meta">
									<span><?php _e('By :','travel');?> <?php the_author(); ?></span>
									<span>&nbsp;I&nbsp;  <?php _e('On : ','travel');?><?php echo esc_html(get_the_time('F j.Y')); ?> </span>
									<span>&nbsp;I&nbsp;  <?php _e('Tagged :','travel');?> <?php the_tags(' '); ?></span>
								</div>
								<p><?php if( ! has_excerpt() ) {
									echo esc_html(wp_trim_words( get_the_content(), 60, '...' )); } 
									else {echo the_excerpt(); }
								?></p>
								<a href="<?php the_permalink(); ?>" class="btn btn-success travel-btn"><?php _e('Read More','travel');?></a>								
								<?php
									wp_link_pages(array(
										'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__('page','travel') . '</span>',
										'after' => '</div>',
										'link_before' => '<span>',
										'link_after' => '</span>',
										'pagelink' => '<span class="screen-reader-text">' . esc_html__('page', 'travel') . '</span>%',
										'separator' => '<span class="screen-reader-text">,</span>',
									));
								?>
							</div><!-- /.post-content -->
							<hr>						
						</article><!-- /.post -->
					<?php endwhile; endif; ?>
					</div><!-- /.blog-posts -->
					<nav aria-label="..." class="text-center">
					  <ul class="travel-pagination pagination">							
						<?php
							// Previous/next page navigation.
							echo paginate_links( array(
								'prev_text'          => __( '<span class="fa fa-angle-left" aria-hidden="true"></span>', 'travel' ),
								'next_text'          => __( '<span class="fa fa-angle-right" aria-hidden="true"></span>', 'travel' ),
							) );
						?>
					  </ul><!-- /.travel-pagination pagination -->
					</nav>
				</div><!-- /.col-md-8 -->
				<?php get_sidebar(); ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div>
	<!-- post-section end -->

	<!-- subscribe start -->
	<?php if (!empty (cs_get_option('newsletter_title'))):?>
	<section id="subscribe">
		<div class="travel-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2><?php echo esc_html(cs_get_option('newsletter_title'));?></h2>
					</div>
					<div class="col-md-offset-1 col-md-5">
						<?php echo do_shortcode('[newsletter]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<!-- subscribe end -->

<?php get_footer();?>