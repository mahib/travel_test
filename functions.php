<?php

if(file_exists(get_template_directory().'/inc/enqueue.php')){
require_once(get_template_directory().'/inc/enqueue.php');
}

if(file_exists(get_template_directory().'/theme-options/cs-framework.php')){
require_once(get_template_directory().'/theme-options/cs-framework.php');
}

if(file_exists(get_template_directory().'/inc/theme_support.php')){
require_once(get_template_directory().'/inc/theme_support.php');
}

if(file_exists(get_template_directory().'/inc/wp-bootstrap-navwalker.php')){
require_once(get_template_directory().'/inc/wp-bootstrap-navwalker.php');
}

if(file_exists(get_template_directory().'/inc/comments_style.php')){
require_once(get_template_directory().'/inc/comments_style.php');
}

if(file_exists(get_template_directory().'/inc/custom_breadcrumbs.php')){
require_once(get_template_directory().'/inc/custom_breadcrumbs.php');
}

if(file_exists(get_template_directory().'/inc/custom_widget.php')){
require_once(get_template_directory().'/inc/custom_widget.php');
}

if(file_exists(get_template_directory().'/inc/tgm-plugin/plugin-activation.php')){
require_once(get_template_directory().'/inc/tgm-plugin/plugin-activation.php');
}

 if(file_exists(get_template_directory().'/inc/theme-options.php')) {
	require_once(get_template_directory().'/inc/theme-options.php');
}

 if(file_exists(get_template_directory().'/inc/demo-importer.php')) {
	require_once(get_template_directory().'/inc/demo-importer.php');
}

 if(file_exists(get_template_directory().'/inc/custom_color.php')) {
	require_once(get_template_directory().'/inc/custom_color.php');
}
